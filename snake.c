#include "graphics.h"

int main() {
    init_graphics();
    int x = 10;
    int y = 10;
    short d = 0;
    char key;
    short kill = 0;
    void* buf = new_offscreen_buffer();
    while(1) {
        //adjust pixel position to wrap around.

        if(x < 0) {
            x = get_width() - 1;
        }
        if(y < 0) {
            y = get_height() - 1;
        }
        if(y == get_height()) {
            y = 0;
        }
        if(x == get_width()) {
            x = 0;
        }

        //draw the pixel
        draw_pixel(buf, x, y, (color_t) RGB(31, 0, 0));
        blit(buf);

        key = getkey();
        switch(key) {
            case 'q':
                kill = 1;
                break;
            case 'w':
                d = 3;
                break;
            case 'd':
                d = 0;
                break;
            case 's':
                d = 1;
                break;
            case 'a':
                d = 2;
                break;
            case 'c':
                clear_screen(buf);
                break;
            default:break;
        }

        if(kill) {
            break;
        }

        switch(d) {
            case 0:
                x++;
                break;
            case 1:
                y++;
                break;
            case 2:
                x--;
                break;
            case 3:
                y--;
                break;
            default:break;
        }
        sleep_ms(20);
    }
    exit_graphics();
}