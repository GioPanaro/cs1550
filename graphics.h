//
// Created by Giovanni Panaro on 9/16/18.
//
#ifndef SNAKE_GRAPHICS_H
#define SNAKE_GRAPHICS_H

typedef unsigned short color_t;

//#define NULL (void *) 0
#define RGB(r, g, b) ((color_t) (r << 11) | (g << 5) | (b))

int get_width();
int get_height();
void init_graphics();
void clear_screen(void *img);
void exit_graphics();
void sleep_ms(long ms);
char getkey();
void draw_pixel(void *img, int x, int y, color_t color);
void draw_line(void *img, int sx, int sy, int fx, int fy, color_t color);
void* new_offscreen_buffer();
void blit(void* osb);


#endif //SNAKE_GRAPHICS_H
