#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <time.h>
#include "graphics.h"

int framebuffer_desc;
color_t *framebuffer;
int size_of_display;
struct termios terminal_settings;
struct fb_var_screeninfo screen_var_info;
struct fb_fix_screeninfo screen_fix_info;

int get_width() {
    return screen_var_info.xres_virtual / 2;
}

int get_height() {
    return screen_var_info.yres_virtual / 2;
}

void clear_screen(void* img) {
    if(img == NULL) {
        write(STDOUT_FILENO, "\033[2J", 4);
        return;
    }
    int i;
    for(i = 0; i <= screen_var_info.yres_virtual; i++) {
        int j;
        for(j = 0; j <= screen_var_info.xres_virtual; j++) {
            draw_pixel(img, j, i, RGB(0, 0, 0));
        }
    }
}

double absv(double n) {
    if(n < 0) {
        n = -n;
    }
    return n;
}

void init_graphics() {
    //open the framebuffer
    framebuffer_desc = open("/dev/fb0", O_RDWR);

    //get screen info
    ioctl(framebuffer_desc, FBIOGET_VSCREENINFO, &screen_var_info);
    ioctl(framebuffer_desc, FBIOGET_FSCREENINFO, &screen_fix_info);

    //turn of ICANON and ECHO
    ioctl(STDIN_FILENO, TCGETS, &terminal_settings);
    terminal_settings.c_lflag &= ~ICANON;
    terminal_settings.c_lflag &= ~ECHO;
    ioctl(STDIN_FILENO, TCSETS, &terminal_settings);

    //set size of display
    size_of_display = screen_var_info.yres_virtual * screen_fix_info.line_length;

    //map framebuffer into memory
    framebuffer = (color_t *) mmap(NULL, (size_t) size_of_display, PROT_WRITE, MAP_SHARED, framebuffer_desc, 0);

    clear_screen(NULL);
}

void exit_graphics() {
    clear_screen(NULL);

    //reset terminal settings
    terminal_settings.c_lflag |= ICANON;
    terminal_settings.c_lflag |= ECHO;
    ioctl(STDIN_FILENO, TCSETS, &terminal_settings);

    //clean 'er up.
    munmap(framebuffer, (size_t) size_of_display);
    close(framebuffer_desc);
}

char getkey() {
    fd_set rfds;
    struct timeval time_wait;
    FD_ZERO(&rfds);
    FD_SET(0, &rfds);
    time_wait.tv_sec = 0;
    time_wait.tv_usec = 0;
    int char_in_buffer = select(STDIN_FILENO + 1, &rfds, NULL, NULL, &time_wait);

    char buffered_char = '\0';

    if (char_in_buffer) {
        read(0, &buffered_char, sizeof(buffered_char));
    }

    return buffered_char;
}

void sleep_ms(long ms) {
    if (ms < 0) {
        return;
    }

    struct timespec sleep_time_struct;
    sleep_time_struct.tv_sec = 0;

    if (ms <= 999) {
        sleep_time_struct.tv_nsec = ms * 1000000;
        nanosleep(&sleep_time_struct, NULL);
    } else {
        while (ms > 0) {
            ms -= 999;
            if (ms > 0) {
                sleep_time_struct.tv_nsec = 999000000;
            } else {
                sleep_time_struct.tv_nsec = (ms + 999) * 1000000;
            }
            nanosleep(&sleep_time_struct, NULL);
        }
    }
}

void draw_pixel(void *img, int x, int y, color_t color) {
    //literally spaghetti code. fixes an issue...
    x = x * 2;
    y = y * 2;
    if(x < 0 || y < 0 || x > screen_var_info.xres_virtual || y > screen_var_info.yres_virtual) {
        return;
    }
    int offset = (y * screen_var_info.xres_virtual) + x;
    color_t *pixel;
    if(img == NULL) {
        pixel = framebuffer + offset;
    } else {
        pixel = img + offset;
    }
    *pixel = color;
}

void draw_line(void *img, int x1, int y1, int x2, int y2, color_t colour) {
    int m = 2 * (y2 - y1);
    int err = m - (x2 - x1);
    int x, y;
    for(x = x1, y = y1; x <= x2; x++) {
        draw_pixel(img, x, y, colour);
        err += m;
        if(err >= 0) {
            y++;
            err -= 2 * (x2 - x1);
        }
    }
}

void* new_offscreen_buffer() {
    return (color_t *) mmap(NULL, (size_t) size_of_display, PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
}

void blit(void* osb) {
    int n = size_of_display;
    char *nosb = (char *) osb;
    char *dst = (char *) framebuffer;
    int i;
    for(i = 0; i < n; i++) {
        dst[i] = nosb[i];
    }
}